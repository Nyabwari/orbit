import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';


import 'InkWellDrawer.dart';

const mainColor = Color(0xfff089225);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffffbfbfd);
const listcolorr = Color(0xfffebecf0);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);
const nyeusi= Color(0xff000000);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat=" ";
String lon=" ";
String alt=" ";
String town=" ";
String online="";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String pesa="ksh";

String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
String pdfurl="";
String shtate="";
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<Literature> literature;
List<Inliterature> inliterature;
List<Holidays> holidays;
final _controller = TextEditingController();
List<Contacts> contacts;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class TasksDashboard extends StatefulWidget {

  _TasksDashboard createState() => _TasksDashboard();

}


class  _TasksDashboard extends State< TasksDashboard> {

  var selected;
  String chkd="";
  String idsss="";

  @override
  void initState() {
    super.initState();
    _restore();
    _determinePosition();
    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));
  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    usertokeni = prefs.getString('StaffId');
    if(prefs.getString('IsOnline')=="false")
    {
      print(prefs.getString('IsOnline'));
      online="Go Online";
    }
    if(prefs.getString('IsOnline')=="true")
    {
      print(prefs.getString('IsOnline'));
      online="Go Offline";
    }
    print("choyy");
    setState(() {
      setState(() {
        usertokeni;
        online;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _online() async {

    pr.show();
    print("sasa");
    String apiUrl = "https://orbit.co.ke/MServer_V1/Online";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"StaffId": usertokeni, "Latitude": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      if(map['IsOnline'].toString()=="true"&&map['Status'].toString()=="true" )
      {
        online="Go Offline";
      }
      setState(() { online; });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }
  Future<void> _edit() async {

    pr.show();
    print("sasa");
    String apiUrl = "https://orbit.co.ke/MServer_V1/EditTask";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"StaffId": usertokeni, "Taskdescription": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      if(map['IsOnline'].toString()=="true"&&map['Status'].toString()=="true" )
      {
        online="Go Offline";
      }
      setState(() { online; });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<void> _offline() async {
    pr.show();
    print("sasa");
    String apiUrl = "https://orbit.co.ke/MServer_V1/Offline";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"StaffId": usertokeni, "Latitude": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      if(map['IsOnline'].toString()=="false"&&map['Status'].toString()=="true" )
      {
        online="Go Online";
      }
      setState(() { online; });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<void> _addTask() async {
    pr.show();
    print("sasa enjo");
    print(idsss);
    print(messo);
    print(usertokeni);
    String apiUrl = "https://orbit.co.ke/MServer_V1/CreateTask";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"staffids": idsss, "Taskdescription": messo, "StaffIdCreatedBy": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("End123");
    print(jsonsDataString);


    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() {
        chkd;
      });
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<void> _editTask() async {
    pr.show();
    print("sasa enjo");
    print(idsss);
    print(messo);
    print(usertokeni);
    String apiUrl = "https://orbit.co.ke/MServer_V1/UpdateTask";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"staffids": idsss, "Taskdescription": messo, "StaffIdCreatedBy": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("End123");
    print(jsonsDataString);


    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() {
        chkd;
      });
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }


  Future<List<Inliterature>> _getUserss() async {

    String apiUrl = "https://orbit.co.ke/MServer_V1/GetTasks";
    Map<String, String> headers = {"Content-type": "application/json" };
    final json =  convert.jsonEncode({  "StaffId": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<Inliterature> alerts = [];

      if (map['TasksView'] != null) {
        print("sawasawa");
        inliterature = new List<Inliterature>();
        map['TasksView'].forEach((v) {
          String complete;
          pesa=v['StaffComment'].toString();
          if(pesa=="null")
          {
            pesa="N/A";

          }
          else
          {
            pesa=v['StaffComment'].toString();

          }
          bool mar=v['IsCompleted'];
          print(mar);
          complete=v['IsCompleted'].toString();
          print("onchwoto "+complete);
          if(complete=="true")
          {
            complete="Yes";

          }
          else
          {
            complete="No";
          }
          Inliterature alert = Inliterature(v["TaskId"].toString(),v["Taskdescription"].toString(),pesa, v["TeamId"].toString(), v["CompanyId"].toString(), v["CompanyName"].toString(), v["StaffName"].toString(), v["TeamName"].toString(), v["StaffId"].toString(), complete, v["CreatedBy"].toString(), v["DateCreated"].toString(), v["IsCompleted"]);

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }





    }
    else {
      print("no");
    }

  }
  Future<List<Contacts>> _getContacts() async {

    String apiUrl = "https://orbit.co.ke/MServer_V1/GetTeamContacts";
    Map<String, String> headers = {"Content-type": "application/json" };
    final json =  convert.jsonEncode({  "StaffId": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<Contacts> alerts = [];

      if (map['PrincipleManagers'] != null) {
        print("sawasawa");
        contacts = new List<Contacts>();
        map['PrincipleManagers'].forEach((v) {

          Contacts alert = Contacts(v["Id"].toString(),v["Name"].toString(), false);

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }





    }
    else {
      print("no");
    }

  }


  ListView _jobsListView(literature) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom:120),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: hesabu,
      itemBuilder: (BuildContext context, int index) {
        return userList(context, index);
      },
    );
  }
  Widget userList(BuildContext context, int index) {

  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    void handleClick(String value) {
      print("rrr");
      print(value);
      switch (value) {
        case 'Go Online':
          _online();
          break;
        case 'Go Offline':
          _offline();
          break;
      }
    }
    return Scaffold(
      drawer: InkWellDrawer(),
      backgroundColor: listcolor,

      appBar: AppBar(

        iconTheme: IconThemeData(
            color: Colors.black
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              chkd="";
              idsss="";
              _controller.clear();
              showDialog(
                context: context,
                builder: (context) {
                  return Dialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    elevation: 16,
                    child: Container(
                      width: MediaQuery.of(context).size.width*90,
                      padding: EdgeInsets.only(top: 20, bottom: 2, right: 20, left: 20),
                      child: Column(
                        mainAxisAlignment : MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Add New Task",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: nyeusi,
                                fontSize: 18
                            ),

                          ),
                          Container(height: 10, color: Colors.transparent),

                          TextFormField(
                            minLines: 5,
                            maxLines: 10,
                            keyboardType: TextInputType.multiline,
                            onChanged: (value) {
                              setState(() {
                                messo = value;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Task Description",
                                alignLabelWithHint: true,
                              hintStyle: TextStyle(
                                  color: nyeusi

                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              ),

                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(5.0),
                                borderSide:  BorderSide(color: mainColor ),

                              ),
                            ),
                          ),
                          Container(height: 20, color: Colors.transparent),
                  StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return
                      TextFormField(
                        minLines: 1,
                        maxLines: 10,
                        controller: _controller,
                        keyboardType: TextInputType.multiline,
                        onChanged: (value) {

                        },
                        readOnly: true,
                        enableInteractiveSelection: true,
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return Dialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                elevation: 16,
                                child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width * .80,
                                  height: MediaQuery
                                      .of(context)
                                      .size
                                      .height * .80,
                                  padding: EdgeInsets.only(
                                      top: 20, bottom: 2, right: 20, left: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "Joints",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: nyeusi,
                                            fontSize: 18
                                        ),

                                      ),
                                      Container(
                                        width: MediaQuery
                                            .of(context)
                                            .size
                                            .width * .65,
                                        height: MediaQuery
                                            .of(context)
                                            .size
                                            .height * .60,

                                        color: mainColor,
                                        child: FutureBuilder(
                                          future: _getContacts(),
                                          builder: (BuildContext context,
                                              AsyncSnapshot snapshot) {
                                            print(snapshot.data);
                                            if (snapshot.data == null) {
                                              return Container(
                                                  color: listcolorr,
                                                  child: Center(
                                                      child: CircularProgressIndicator()
                                                  )
                                              );
                                            }
                                            else {
                                              return
                                                Container(

                                                  color: listcolorr,
                                                  child: ListView.builder(
                                                    padding: EdgeInsets.only(
                                                        bottom: 300, top: 5),
                                                    itemCount: snapshot.data
                                                        .length,
                                                    itemBuilder: (
                                                        BuildContext context,
                                                        int index) {
                                                      return Container(
                                                        color: mainColor,
                                                        child: Card(
                                                          color: listcolorr,
                                                          margin: EdgeInsets
                                                              .all(0.5),
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: BorderRadius
                                                                  .circular(
                                                                  10)),
                                                          elevation: 4.0,
                                                          child: Container(
                                                            decoration: BoxDecoration(
                                                              color: listcolorr,
                                                              borderRadius: BorderRadius
                                                                  .circular(40),
                                                            ),
                                                            child: StatefulBuilder(
                                                              builder: (
                                                                  BuildContext context,
                                                                  StateSetter setState) {
                                                                return CheckboxListTile(
                                                                  title: Text(
                                                                      snapshot
                                                                          .data[index]
                                                                          .Name),
                                                                  controlAffinity: ListTileControlAffinity
                                                                      .leading,
                                                                  value: snapshot
                                                                      .data[index]
                                                                      .selected,

                                                                  activeColor: mainColor,
                                                                  onChanged: (
                                                                      bool value) {
                                                                    setState(() {
                                                                      if (value ==
                                                                          true) {
                                                                        snapshot
                                                                            .data[index]
                                                                            .selected =
                                                                            value;

                                                                        if(idsss=="")
                                                                          {
                                                                            idsss=snapshot .data[index] .Id;
                                                                            chkd=snapshot .data[index] .Name;
                                                                          }
                                                                        else
                                                                          {
                                                                            idsss =idsss +"," + snapshot .data[index] .Id ;
                                                                            chkd = chkd +","+ snapshot .data[index] .Name;
                                                                          }


                                                                        print(
                                                                            chkd);
                                                                        print(
                                                                            idsss);
                                                                        _controller.text = chkd;
                                                                      }
                                                                      else {
                                                                        snapshot
                                                                            .data[index]
                                                                            .selected =
                                                                            value;
                                                                      }
                                                                    });
                                                                  },
                                                                );
                                                              },
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                );
                                            }
                                          },
                                        ),


                                      ),

                                      Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment
                                              .spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment
                                              .baseline,

                                          children: <Widget>[
                                            Container(width: 15,
                                                color: Colors.transparent),
                                            Expanded(

                                              child: new RaisedButton(

                                                elevation: 5.0,
                                                color: mainColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius
                                                      .circular(10.0),
                                                ),

                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Column(

                                                  children: <Widget>[
                                                    Text(
                                                      "Choose",
                                                      textAlign: TextAlign
                                                          .center,
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: MediaQuery
                                                            .of(context)
                                                            .size
                                                            .height / 50,
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ),

                                            ),
                                            Container(width: 20,
                                                color: Colors.transparent),
                                            Expanded(

                                              child: new RaisedButton(
                                                elevation: 5.0,
                                                color: Colors.grey,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius
                                                      .circular(10.0),
                                                ),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Column(

                                                  children: <Widget>[

                                                    Text(
                                                      " Cancel",
                                                      textAlign: TextAlign
                                                          .center,
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: MediaQuery
                                                            .of(context)
                                                            .size
                                                            .height / 40,
                                                      ),

                                                    ),

                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(width: 15,
                                                color: Colors.transparent),

                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                        decoration: InputDecoration(
                          labelText: "Staffs",
                          hintStyle: TextStyle(
                              color: nyeusi

                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(5.0)),
                          ),

                          focusedBorder: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: BorderSide(color: mainColor),

                          ),
                        ),
                      );
                  }
                  ),
                          Container(width: 5, color: Colors.transparent),
                          Spacer(),
                          Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.baseline,

                              children: <Widget>[
                                Container(width: 15, color: Colors.transparent),
                                Expanded(

                                  child:   new RaisedButton(

                                    elevation: 5.0,
                                    color: mainColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),

                                    onPressed: () {
                                    _addTask();
                                    },
                                    child: Column(

                                      children: <Widget>[
                                        Text(
                                          "Add",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: MediaQuery
                                                .of(context)
                                                .size
                                                .height / 50,
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),

                                ),
                                Container(width: 20, color: Colors.transparent),
                                Expanded(

                                  child:    new RaisedButton(
                                    elevation: 5.0,
                                    color: Colors.grey,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Column(

                                      children: <Widget>[

                                        Text(
                                          " Cancel",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: MediaQuery
                                                .of(context)
                                                .size
                                                .height / 40,
                                          ),

                                        ),

                                      ],
                                    ),
                                  ),
                                ),
                                Container(width: 15, color: Colors.transparent),

                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              );


            },
          ),

          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return {online}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),

        ],
        centerTitle: true,
        backgroundColor: listcolorr,
        title: Text(
          "Tasks",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,

          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: mainColor,
          child: FutureBuilder(
            future: _getUserss(),
            builder: (BuildContext context, AsyncSnapshot snapshot){
              print(snapshot.data);
              if(snapshot.data == null){
                return Container(
                    color:listcolorr,
                    child: Center(
                        child: CircularProgressIndicator()
                    )
                );
              }
              else {
                return
                  Container(
                    color:listcolorr,
                    child: ListView.builder(
                      padding: EdgeInsets.only(bottom:300, top:5),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          color: mainColor,
                          child: Card(
                            color: listcolorr,
                            margin: EdgeInsets.all(0.5),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4.0,
                            child: Container(
                                padding: EdgeInsets.only(right:8),

                                decoration: BoxDecoration(color:listcolorr,
                                  borderRadius: BorderRadius.circular(40),
                                ),
                                child: ListTile(
                                  contentPadding: EdgeInsets.symmetric(horizontal: 5.0),


                                  title: Container(
                                    padding: EdgeInsets.only(right: 15.0, left:13.0),


                                    child: Text(
                                      snapshot.data[index].Taskdescription,
                                      style: TextStyle(
                                        color:nyeusi,
                                        fontFamily: 'Montserrat-Regular',
                                        decoration: TextDecoration.underline,
                                        fontWeight: FontWeight.bold,


                                      ),
                                    ),

                                  ),
                                  subtitle: Container(
                                    padding: EdgeInsets.only(right: 15.0, top: 5, left:15.0),

                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(height: 3, color: Colors.transparent),

                                        Text.rich(
                                          TextSpan(
                                            children: [

                                              TextSpan(text: "Comments: ",
                                                style: TextStyle(
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',


                                                ),),
                                              TextSpan(

                                                text: snapshot.data[index].StaffComment,

                                                style: TextStyle(fontWeight: FontWeight.bold,
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(height: 5, color: Colors.transparent),

                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              TextSpan(text: "By: ",
                                                style: TextStyle(
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',


                                                ),),
                                              TextSpan(
                                                text: snapshot.data[index].CreatedBy,

                                                style: TextStyle(fontWeight: FontWeight.bold,
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(height: 5, color: Colors.transparent),

                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              TextSpan(text: "Completed: ",
                                                style: TextStyle(
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',


                                                ),),
                                              TextSpan(
                                                text: snapshot.data[index].IsCompleted,

                                                style: TextStyle(
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(height: 5, color: Colors.transparent),

                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              WidgetSpan(
                                                child: Icon(Icons.date_range,size: 14, color:mainColor),
                                              ),
                                              TextSpan(text: "Created On: ",
                                                style: TextStyle(
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',


                                                ),),
                                              TextSpan(

                                                text: snapshot.data[index].DateCreated,

                                                style: TextStyle(fontWeight: FontWeight.bold,
                                                  color:nyeusi,
                                                  fontFamily: 'Montserrat-Regular',),
                                              ),
                                            ],
                                          ),
                                        ),

                                        Container(height: 8, color: Colors.transparent),

                                      ],
                                    ),
                                  ),
                                    trailing: Icon(Icons.edit, color: mainColor,),
                                    onTap: () {
                                      showDialog(
                                      context: context,
                                      builder: (context) {
                                      return Dialog(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      elevation: 16,
                                      child: Container(
                                        
                                      height: 290.0,
                                      width: 360.0,
                                        padding: EdgeInsets.only(top: 20, bottom: 2, right: 20, left: 20),
                                        child: Column(
                                          mainAxisAlignment : MainAxisAlignment.center,
                                          crossAxisAlignment : CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "Task",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              color: nyeusi,
                                                fontSize: 18
                                            ),

                                          ),
                                          TextFormField(
                                            minLines: 5,
                                            maxLines: 10,
                                            keyboardType: TextInputType.multiline,
                                              initialValue:snapshot.data[index].Taskdescription,
                                            onChanged: (value) {
                                              setState(() {
                                               messo = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              hintText: 'Task',
                                              hintStyle: TextStyle(
                                                  color: nyeusi

                                              ),
                                              border: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                              ),

                                              focusedBorder: new OutlineInputBorder(
                                                borderRadius: new BorderRadius.circular(5.0),
                                                borderSide:  BorderSide(color: mainColor ),

                                              ),
                                            ),
                                          ),
                                            StatefulBuilder(
                                          builder: (BuildContext context, StateSetter setState) {
                                            return CheckboxListTile(
                                            title: Text("Completed"),
                                            controlAffinity: ListTileControlAffinity.leading,
                                            value: snapshot.data[index].selected,
                                            activeColor: mainColor,
                                              onChanged: (bool value) {
                                              setState(() {
                                                if(value==true)
                                                {
                                                  snapshot.data[index].selected = value;
                                                //  chkd=chkd+snapshot.data[index].responseId.toString()+",";
                                                 // print("Enchogu");
                                                  //print(chkd);
                                                }
                                                else
                                                {
                                                  snapshot.data[index].selected = value;
                                                }
                                              });
                                            },
                                            );
                                          },
                                          ),
                                          Center(
                                           child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Container(width: 15, color: Colors.transparent),
                                                Expanded(

                                                  child:   new RaisedButton(

                                                    elevation: 5.0,
                                                    color: mainColor,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(10.0),
                                                    ),

                                                    onPressed: () {
                                                     // _doSignIn();
                                                    },
                                                    child: Column(

                                                      children: <Widget>[
                                                        Text(
                                                          "Edit",
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: MediaQuery
                                                                .of(context)
                                                                .size
                                                                .height / 50,
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                  ),

                                                ),
                                                Container(width: 20, color: Colors.transparent),
                                                Expanded(

                                                  child:    new RaisedButton(
                                                    elevation: 5.0,
                                                    color: Colors.grey,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(10.0),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: Column(

                                                      children: <Widget>[

                                                        Text(
                                                          " Cancel",
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: MediaQuery
                                                                .of(context)
                                                                .size
                                                                .height / 40,
                                                          ),

                                                        ),

                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(width: 15, color: Colors.transparent),

                                              ],
                                            ),
                                          )
                                        ],
                                        ),
                                      ),
                                      );
                                      },
                                      );


                                    }
                                )
                            ),
                          ),
                        );
                      },
                    ),
                  );
              }
            },
          ),


        ),
      ),


    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}

class PDFViewerFromUrl extends StatelessWidget {
  const PDFViewerFromUrl({Key key, @required this.url}) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(pdfurl),
      ),
      body: const PDF().fromUrl(
        url,
        placeholder: (double progress) => Center(child: Text('$progress %')),
        errorWidget: (dynamic error) => Center(child: Text(error.toString())),
      ),
    );
  }
}


class Literature {
  String LiteratureName;
  String LiteratureId;
  String LiteratureUrl;
  String LiteratureDescription;

  Literature(this.LiteratureName, this.LiteratureId, this.LiteratureUrl, this.LiteratureDescription);

}
class Inliterature {
  String TaskId;
  String Taskdescription;
  String StaffComment;
  String TeamId;
  String CompanyId;
  String CompanyName;
  String StaffName;
  String TeamName;
  String StaffId;
  String IsCompleted;
  String CreatedBy;
  String DateCreated;
  var selected;
  Inliterature(this.TaskId, this.Taskdescription,  this.StaffComment, this.TeamId, this.CompanyId,  this.CompanyName,  this.StaffName,  this.TeamName,  this.StaffId,  this.IsCompleted,  this.CreatedBy,  this.DateCreated,  this.selected);
}
class Holidays {
  String HolidayId;
  String HolidayName;
  String HolidayDescription;
  String HolidayStartDateTime;
  String HolidayEndDateTime;

  Holidays(this.HolidayId, this.HolidayName, this.HolidayDescription, this.HolidayStartDateTime,  this.HolidayEndDateTime);
}

class Contacts {
  String Id;
  String Name;
  var selected;
  Contacts(this.Id, this.Name, this.selected);
}
