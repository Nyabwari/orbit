import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:flutter_session/flutter_session.dart';
import 'package:insight/Calender.dart';
import 'package:insight/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';

import 'ClientDashboard.dart';
import 'Leaves.dart';
import 'Orders.dart';
import 'Locate.dart';
import 'Tasks.dart';


const mainColor = Color(0xfff089225);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const mwanzo = Color(0xffa7b3bd);
const mwisho = Color(0xffeaecf0);
String jina="  ", kampuni="  ", team="  ", picha;
SharedPreferences prefs;
String _platformVersion = 'Unknown';


class InkWellDrawer extends StatefulWidget {
  _InkWellDrawer createState() => _InkWellDrawer();

}

class  _InkWellDrawer extends State< InkWellDrawer> {

  @override
  void initState() {
    super.initState();
    _restore();
    initPlatformState();
  }
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterOpenWhatsapp.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }
  _restore() async {
    print('restoring...');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      jina = prefs.getString('StaffNames').toString();
      kampuni = prefs.getString('CompanyName').toString();
      team = prefs.getString('TeamName').toString();
      picha = prefs.getString('IconUrl').toString();

    });


  }
  Widget _createHeader() {
    return SizedBox(
      height : 200.0,
      child  : new DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.only(right: 20,left: 20, top: 20, bottom: 20),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.topRight,
              colors: [mwanzo,mwisho]
          ),
        ),

        child: Stack(

          children: <Widget>[
            Align(
              alignment: Alignment.topLeft + Alignment(.3, -.7),
              child:  Container(

             child: Container(
               height: MediaQuery.of(context).size.height*0.15,
               width: MediaQuery.of(context).size.width,
               decoration: BoxDecoration(
                 image: DecorationImage(
                   image: NetworkImage(picha),
                 ),
               ),
             ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft+ Alignment(.3, -.6),
              child: Text(
                jina,
                style: TextStyle(color: Colors.black, fontSize: 18.0),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft + Alignment(.3, -.25),
              child: Text(
                kampuni,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft + Alignment(.3, .05),
              child: Text(
                team,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }
  @override
  Widget build (BuildContext ctxt) {

    return new Drawer(
      child: Container(
        color: tabcolor,
      child: ListView(
        children: <Widget>[
          _createHeader(),
          CustomListTile(Icons.dashboard, 'Dashboard', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
                builder: (context) => new ClientDashboard())
            )
          }),

          CustomListTile(Icons.list, 'Tasks', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
          builder: (context) => new TasksDashboard())
          )
          }),
          CustomListTile(Icons.format_list_numbered, 'Orders', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
                builder: (context) => new OrdersDashboard())
            )          }),
          CustomListTile(Icons.event_available, 'Calender', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
                builder: (context) => new CalenderPage())
            )
          }),
          CustomListTile(Icons.date_range, 'Leaves', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
                builder: (context) => new LeavesDashboard())
            )
          }),
          CustomListTile(Icons.contacts, 'Contacts & Activity', ()=>{

          }),
          CustomListTile(Icons.location_on, 'Locate Staff', ()=>{
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
                builder: (context) => new LocateDashboard())
            )
          }),
          CustomListTile(Icons.sync, 'Sync', ()=>{
            Navigator.pop(ctxt),
          }),
          CustomListTile(Icons.vertical_split_rounded, 'About', ()=>{
            _launchRate(),
          }),
          CustomListTile(Icons.logout, 'LogOut', ()=>{
            _launchBug(),
            Navigator.pop(ctxt),
            Navigator.push(context, new MaterialPageRoute(
                builder: (context) => new LoginPage())
            )
          }),

          Container(height: 20, color: Colors.transparent),
          Spacer(),
          Center(
            child: Text('Version 1.3'),

          ),
        ],

      ),
      ),
    );
  }
  _launchPolicy() async {
    const url = 'http://alerts.syve.co.ke/termandconditions';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchRate() async {
    const url = 'https://orbit.co.ke/#about';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchBug() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("StaffId", null);
  }
  _launchContact() async {

    await launch(
        "https://wa.me/${254795749348}?text=Hello...");
  }
}


class CustomListTile extends StatelessWidget{

  final IconData icon;
  final  String text;
  final Function onTap;

  CustomListTile(this.icon, this.text, this.onTap);
  @override
  Widget build(BuildContext context){
    //ToDO
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child:Container(
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey.shade400))
        ),
        child: InkWell(
            splashColor: Colors.orangeAccent,
            onTap: onTap,
            child: Container(
                height: 40,
                child: Row(
                  mainAxisAlignment : MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Icon(icon),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                      ),
                      Text(text, style: TextStyle(
                          fontSize: 16
                      ),),
                    ],),

                    Icon(Icons.arrow_right)
                  ],)
            )
        ),
      ),
    );
  }
}