import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';


import 'InkWellDrawer.dart';

const mainColor = Color(0xfff089225);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffffbfbfd);
const listcolorr = Color(0xfffebecf0);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);
const nyeusi= Color(0xff000000);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat=" ";
String lon=" ";
String alt=" ";
String town=" ";
String online="";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String pesa="ksh";

String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
String pdfurl="";
String shtate="";
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<Literature> literature;
List<Inliterature> inliterature;
List<Holidays> holidays;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class ClientDashboard extends StatefulWidget {

  _ClientDashboard createState() => _ClientDashboard();

}


class  _ClientDashboard extends State< ClientDashboard> {



  @override
  void initState() {
    super.initState();
    _restore();
    _determinePosition();
    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));
  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    usertokeni = prefs.getString('StaffId');
    if(prefs.getString('IsOnline')=="false")
    {
      print(prefs.getString('IsOnline'));
      online="Go Online";
    }
    if(prefs.getString('IsOnline')=="true")
    {
      print(prefs.getString('IsOnline'));
      online="Go Offline";
    }
    print("choyy");
    _getUsers();
    setState(() {
      setState(() {
        usertokeni;
        online;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _online() async {

    pr.show();
    print("sasa");
    String apiUrl = "https://orbit.co.ke/MServer_V1/Online";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"StaffId": usertokeni, "Latitude": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      if(map['IsOnline'].toString()=="true"&&map['Status'].toString()=="true" )
        {
          online="Go Offline";
        }
      setState(() { online; });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<void> _offline() async {
    pr.show();
    print("sasa");
    String apiUrl = "https://orbit.co.ke/MServer_V1/Offline";
    Map<String, String> headers = {"Content-type": "application/json","Accept": "application/json" };
    final json =  convert.jsonEncode({"StaffId": usertokeni, "Latitude": lat, "Longitude": lon});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      if(map['IsOnline'].toString()=="false"&&map['Status'].toString()=="true" )
      {
        online="Go Online";
      }
      setState(() { online; });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<List<Literature>> _getUsers() async {

    String apiUrl = "https://orbit.co.ke/MServer_V1/GetMyDetails";
    Map<String, String> headers = {"Content-type": "application/json" };
    final json =  convert.jsonEncode({  "StaffId": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    print("Endmonth");
    print(usertokeni);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString("StaffNames", map['StaffNames'].toString());
      prefs.setString("UserId", map['UserId'].toString());
      prefs.setString("CompanyId", map['CompanyId'].toString());
      prefs.setString("CompanyName", map['CompanyName'].toString());
      prefs.setString("TeamId", map['TeamId'].toString());
      prefs.setString("TeamName", map['TeamName'].toString());
      prefs.setString("PriceCurrency", map['PriceCurrency'].toString());
      prefs.setString("GPSMinutes",map['GPSMinutes'].toString());
      prefs.setString("IsOnline", map['IsOnline'].toString());
      prefs.setString("LatestCallsMade", map['LatestCallsMade'].toString());
      prefs.setString("IconUrl", map['IconUrl'].toString());
      prefs.setString("UserRole", map['UserRole'].toString());

      List<Literature> alerts = [];

      if (map['Literature'] != null) {
        print("sawasawa");
        print(map['Literature'].toString());
        literature = new List<Literature>();
        map['Literature'].forEach((v) {


          Literature alert = Literature(v["LiteratureName"].toString(),v["LiteratureId"].toString(),v["LiteratureUrl"].toString(), v["LiteratureDescription"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }


    }
    else {
      print("no");
    }

  }
  Future<List<Inliterature>> _getUserss() async {

    String apiUrl = "https://orbit.co.ke/MServer_V1/GetMyDetails";
    Map<String, String> headers = {"Content-type": "application/json" };
    final json =  convert.jsonEncode({  "StaffId": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<Inliterature> alerts = [];
        pesa=map['PriceCurrency'].toString();
      if (map['Productdetails'] != null) {
        print("sawasawa");
        inliterature = new List<Inliterature>();
        map['Productdetails'].forEach((v) {



          Inliterature alert = Inliterature(v["ProductId"].toString(),v["ProductName"].toString(),v["ProductDescription"].toString(), v["RecommendedRetailPrice"].toString(), v["TradePrice"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }





    }
    else {
      print("no");
    }

  }

  Future<List<Holidays>> _getHolidays() async {

    String apiUrl = "https://orbit.co.ke/MServer_V1/GetMyDetails";
    Map<String, String> headers = {"Content-type": "application/json" };
    final json =  convert.jsonEncode({  "StaffId": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<Holidays> alerts = [];

      if (map['Holidays'] != null) {
        print("sawasawa");
        holidays = new List<Holidays>();
        map['Holidays'].forEach((v) {



          Holidays alert = Holidays(v["HolidayId"].toString(),v["HolidayName"].toString(),v["HolidayDescription"].toString(), v["HolidayStartDateTime"].toString(), v["HolidayEndDateTime"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }





    }
    else {
      print("no");
    }

  }

  ListView _jobsListView(literature) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom:120),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: hesabu,
      itemBuilder: (BuildContext context, int index) {
        return userList(context, index);
      },
    );
  }
  Widget userList(BuildContext context, int index) {

  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });
    void handleClick(String value) {
      print("rrr");
      print(value);
      switch (value) {
        case 'Go Online':
          _online();
          break;
        case 'Go Offline':
          _offline();
          break;
      }
    }
    return Scaffold(
        drawer: InkWellDrawer(),
      backgroundColor: listcolor,

      appBar: AppBar(

        iconTheme: IconThemeData(
            color: Colors.black
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return {online}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),

        ],
        centerTitle: true,
        backgroundColor: listcolorr,
        title: Text(
          "Dashboard",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,

          ),
        ),
      ),
        body: SingleChildScrollView(
          child: Stack(

            children: <Widget>[

              Column(
                crossAxisAlignment: CrossAxisAlignment.center,

                children: <Widget>[

                  DefaultTabController(
                    length: 3,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          color: gray,

                          child: Material(
                            color: gray,
                            child: TabBar(
                              indicatorColor: mainColor,
                              labelColor: mainColor,
                              unselectedLabelColor: nyeusi,



                              tabs: [
                                Tab(child:  Text(
                                  "LITERATURE",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                  ),

                                ),
                                ),
                                Tab(child:  Text(
                                  "PRODUCTS",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                  ),

                                ),
                                ),
                                Tab(child:  Text(
                                  "HOLIDAYS",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                  ),

                                ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          //Add this to give height
                          height: MediaQuery.of(context).size.height,
                          child: TabBarView(children: [
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getUsers(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        color:listcolorr,
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  } else {
                                    return
                                    Container(
                                      color:listcolorr,
                                      child: ListView.builder(
                                      padding: EdgeInsets.only(bottom:300, top:5),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Container(
                                          color: mainColor,
                                            child: Card(
                                          color: tabcolor,
                                          margin: EdgeInsets.all(0.5),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                          elevation: 4.0,
                                          child: Container(
                                              padding: EdgeInsets.only(right:12.5),

                                              decoration: BoxDecoration(color: listcolorr,
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                              child: ListTile(
                                                contentPadding: EdgeInsets.symmetric(horizontal: 6.0),


                                                title: Container(
                                                  padding: EdgeInsets.only(right: 15.0, left:15.0),


                                                  child: Text(
                                                    snapshot.data[index].LiteratureName,
                                                    style: TextStyle(
                                                        color:nyeusi,
                                                        fontFamily: 'Montserrat-Regular',
                                                      fontWeight: FontWeight.bold,

                                                    ),
                                                  ),

                                                ),

                                                // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
                                                subtitle: Container(
                                                  padding: EdgeInsets.only(right: 15.0, top: 5,bottom:5, left:15.0),


                                                  child: Text(
                                                    snapshot.data[index].LiteratureDescription,
                                                    style: TextStyle(
                                                        color:nyeusi,
                                                        fontFamily: 'Montserrat-Regular'

                                                    ),
                                                  ),

                                                ),
                                                trailing: Icon(Icons.file_download, color: mainColor,),
                                                  onTap: () {
                                                  pdfurl=snapshot.data[index].LiteratureName;
                                                    print("nimefinywa uuzi");
                                                    print(snapshot.data[index].LiteratureUrl);
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute<dynamic>(
                                                        builder: (_) => PDFViewerFromUrl(
                                                          url: snapshot.data[index].LiteratureUrl,
                                                        ),
                                                      ),
                                                    );


                                                            }



                                              )
                                        ),
                                            ),
                                        );
                                      },
                                      ),
                                    );
                                  }
                                },
                              ),


                            ),
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getUserss(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        color:listcolorr,
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  }
                                  else {
                                    return
                                      Container(
                                        color:listcolorr,
                                        child: ListView.builder(
                                          padding: EdgeInsets.only(bottom:300, top:5),
                                          itemCount: snapshot.data.length,
                                          itemBuilder: (BuildContext context, int index) {
                                            return Container(
                                              color: mainColor,
                                              child: Card(
                                                color: listcolorr,
                                                margin: EdgeInsets.all(0.5),
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                elevation: 4.0,
                                                child: Container(
                                                    padding: EdgeInsets.only(right:12.5),

                                                    decoration: BoxDecoration(color:listcolorr,
                                                      borderRadius: BorderRadius.circular(40),
                                                    ),
                                                    child: ListTile(
                                                      contentPadding: EdgeInsets.symmetric(horizontal: 5.0),


                                                      title: Container(
                                                        padding: EdgeInsets.only(right: 15.0, left:13.0),


                                                        child: Text(
                                                          snapshot.data[index].ProductName,
                                                          style: TextStyle(
                                                            color:nyeusi,
                                                            fontFamily: 'Montserrat-Regular',
                                                            decoration: TextDecoration.underline,
                                                            fontWeight: FontWeight.bold,


                                                          ),
                                                        ),

                                                      ),
                                                      subtitle: Container(
                                                        padding: EdgeInsets.only(right: 15.0, top: 5, left:15.0),

                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
                                                            Container(height: 3, color: Colors.transparent),

                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  WidgetSpan(
                                                                    child: Icon(Icons.money,size: 14, color:mainColor),
                                                                  ),
                                                                  TextSpan(text: "Trade Price: ",
                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(

                                                                    text: pesa+snapshot.data[index].TradePrice,

                                                                    style: TextStyle(fontWeight: FontWeight.bold,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 5, color: Colors.transparent),
                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  WidgetSpan(
                                                                    child: Icon(Icons.money,size: 14, color:mainColor),
                                                                  ),
                                                                  TextSpan(text: "Recommended Retail Price: ",
                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: pesa+snapshot.data[index].RecommendedRetailPrice,

                                                                    style: TextStyle(fontWeight: FontWeight.bold,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 5, color: Colors.transparent),

                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  TextSpan(text: "Description: ",
                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: snapshot.data[index].ProductDescription,

                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 8, color: Colors.transparent),



                                                          ],
                                                        ),
                                                      ),

                                                    )
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                  }
                                },
                              ),


                            ),
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getHolidays(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        color:listcolorr,
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  }
                                  else {
                                    return
                                      Container(
                                        color:listcolorr,
                                        child: ListView.builder(
                                          padding: EdgeInsets.only(bottom:300, top:5),
                                          itemCount: snapshot.data.length,
                                          itemBuilder: (BuildContext context, int index) {
                                            return Container(
                                              color: mainColor,
                                              child: Card(
                                                color: listcolorr,
                                                margin: EdgeInsets.all(0.5),
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                elevation: 4.0,
                                                child: Container(
                                                    decoration: BoxDecoration(color:listcolorr,
                                                      borderRadius: BorderRadius.circular(40),
                                                    ),
                                                    child: ListTile(
                                                      contentPadding: EdgeInsets.symmetric(horizontal: 5.0),


                                                      title: Container(
                                                        padding: EdgeInsets.only(right: 15.0, left:13.0),


                                                        child: Text(
                                                          snapshot.data[index].HolidayName,
                                                          style: TextStyle(
                                                            color:nyeusi,
                                                            fontFamily: 'Montserrat-Regular',
                                                            decoration: TextDecoration.underline,
                                                            fontWeight: FontWeight.bold,


                                                          ),
                                                        ),

                                                      ),
                                                      subtitle: Container(
                                                        padding: EdgeInsets.only(right: 15.0, top: 5, left:15.0),

                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
                                                            Container(height: 3, color: Colors.transparent),

                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  WidgetSpan(
                                                                    child: Icon(Icons.date_range,size: 14, color:mainColor),
                                                                  ),
                                                                  TextSpan(text: "From: ",
                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(

                                                                    text: snapshot.data[index].HolidayStartDateTime,

                                                                    style: TextStyle(fontWeight: FontWeight.bold,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 5, color: Colors.transparent),

                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  WidgetSpan(
                                                                    child: Icon(Icons.date_range,size: 14, color:mainColor),
                                                                  ),
                                                                  TextSpan(text: "To: ",
                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: snapshot.data[index].HolidayEndDateTime,

                                                                    style: TextStyle(fontWeight: FontWeight.bold,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 5, color: Colors.transparent),

                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  TextSpan(text: "Description: ",
                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: snapshot.data[index].HolidayDescription,

                                                                    style: TextStyle(
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),

                                                            Container(height: 8, color: Colors.transparent),


                                                          ],
                                                        ),
                                                      ),

                                                    )
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                  }
                                },
                              ),


                            ),

                          ]),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ],

          ),
        ),


    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}

class PDFViewerFromUrl extends StatelessWidget {
  const PDFViewerFromUrl({Key key, @required this.url}) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(pdfurl),
      ),
      body: const PDF().fromUrl(
        url,
        placeholder: (double progress) => Center(child: Text('$progress %')),
        errorWidget: (dynamic error) => Center(child: Text(error.toString())),
      ),
    );
  }
}


class Literature {
  String LiteratureName;
  String LiteratureId;
  String LiteratureUrl;
  String LiteratureDescription;

  Literature(this.LiteratureName, this.LiteratureId, this.LiteratureUrl, this.LiteratureDescription);

}
class Inliterature {
  String ProductId;
  String ProductName;
  String ProductDescription;
  String RecommendedRetailPrice;
  String TradePrice;

  Inliterature(this.ProductId, this.ProductName, this.ProductDescription, this.RecommendedRetailPrice,  this.TradePrice);
}
class Holidays {
  String HolidayId;
  String HolidayName;
  String HolidayDescription;
  String HolidayStartDateTime;
  String HolidayEndDateTime;

  Holidays(this.HolidayId, this.HolidayName, this.HolidayDescription, this.HolidayStartDateTime,  this.HolidayEndDateTime);
}
